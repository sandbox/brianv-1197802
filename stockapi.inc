<?php

/**
 * @file
 * A wrapper API for the retreiving stock quotes from Yahoo! Finance.
 */

/**
 * Implement a HTTP request to Yahoo Finance to retrieve stock quotes.
 *
 * @param $symbols array
 *   An array of ticker symbols to send to Yahoo!. There is not limit on the
 *   number of symbols. Yahoo! limits 200 symbols per request, so we queue up
 *   the request into batches.
 *
 * @return
 *   An array of stocks with stock information fromm Yahoo!. False on failure.
 */
function stockapi_fetch($symbols) {

  // Step 1: Yahoo! limits stock updates to 200 or less per request. Split up larger arrays.
  $l = 199;
  if (count($symbols) <= $l) {
    $s = array_map('urlencode', $symbols); $s = implode('+', $s);
    $fields = stockapi_get_quotetype(variable_get('stockapi_quotetype', 'basic'));

    $url = "http://download.finance.yahoo.com/d/quotes.csvr?s=$s&f=$fields&e=.csv";
    $results = drupal_http_request($url);
    if (_stockapi_request_failure($results)) return FALSE;

    // Symbols are returned in CSV format.
    return _stockapi_parse_csv($results->data);
  }
  else {
    $batches = array_chunk($symbols, $l);
    $stocks = array();
    foreach ($batches as $key => $batch) {
      $stocks = array_merge(stockapi_fetch($batch), $stocks);
    }
    return $stocks;
  }
}

/**
 * Save a single stock quote to the database.
 *
 * @param $stock object
 *   The stock object keys' must match the field names of the database table.
 *   The stock object values' are the data to insert.
 *
 * @return
 *   TRUE on success, FALSE on failure
 */
function stockapi_save($stock) {

  // Fix potential text in float fields.
  $stock = _stockapi_fix_floats($stock);

  if (db_query("SELECT symbol FROM {stockapi} WHERE symbol = :symbol", array(':symbol' => $stock->symbol))->fetchField()) {
    // Update

    $stock = (array)$stock;
    unset($stock['symbol']); // Primary key

    $updated = db_update('stockapi')
      ->fields((array)$stock)
      ->condition('symbol', $stock->symbol)
      ->execute();

    return $updated;
  }
  else {
    //Insert
    $inserted = db_insert('stockapi')
      ->fields((array)$stock)
      ->execute();

    return $inserted;
  }
}

/**
 * Return ticker information for a single symbol from our database, not Yahoo!
 *
 * @param $symbol string
 *   The ticker symbol to retrieve information for.
 * @param $q string
 *   The type of information to retrieve. Can be either: basic, extended,
 *   or realtime. The default is the site-wide setting, which is usually the
 *   best bet here.
 * @return object
 */
function stockapi_load($symbol, $q = NULL) {
  static $stocks = array();

  if (!isset($stocks[$symbol])) {
    $fields = _stockapi_get_fields($q);
    $results = db_query("SELECT " . implode(',', $fields) . " FROM {stockapi} WHERE symbol = :symbol", array(':symbol' => $symbol));
    foreach($results as $result) {
      $stocks[$symbol] = $result;
    }
  }

  return $stocks[$symbol];
}

/**
 * Return ticker information for a batch of symbols from our database, not Yahoo!
 *
 * @param $symbol array
 *   An array of ticker symbols to retrieve information for.
 * @param $q string
 *   The type of information to retrieve. Can be either: basic, extended,
 *   or realtime. The default is the site-wide setting, which is usually the
 *   best bet here.
 * @return array
 *   An array of stocks, keyed by their ticker symbol. Each stock is an object within the array.
 */
function stockapi_multiload($symbol, $q = NULL) {
  $stocks = array();

  $fields = _stockapi_get_fields($q);
  $symbols = implode(', ', array_map('_stockapi_quote_it', $symbol));
  $results = db_query('SELECT ' . implode(', ', $fields) . ' FROM {stockapi} WHERE symbol IN (:symbols)', array(':symbols' => $symbols));
  foreach ($results as $stock) {
    $stocks[$stock->symbol] = $stock;
  }

  return $stocks;
}

/**
 * Wrap a string in quotes. Usually used with array_map().
 */
function _stockapi_quote_it($string, $style = 'single') {
  return ($style != 'single') ? '"' . $string . '"' : "'" . $string . "'";
}

/**
 * Return the field parameter string to pull the correct columns from Yahoo!
 */
function stockapi_get_quotetype($q = NULL) {
  $quotetype['basic']    = 'snl1d1t1c1p2va2bapomwerr1dyj1x';
  $quotetype['extended'] = $quotetype['basic'] . 's7t8e7e8e9r6r7r5b4p6p5j4m3m4';
  $quotetype['realtime'] = $quotetype['basic'] . 'b2b3k2k1c6m2j3';

  if ($quotetype[$q]) {
    return $quotetype[$q];
  }
  // Always return the basic set as a fall through.
  return $quotetype['basic'];
}

/**
 * Convert a stock from Yahoo! to its obect oriented counterpart.
 *
 * @return object
 */
function _stockapi_to_object($stock, $q = NULL) {
  $fields = _stockapi_get_fields($q);

  $ns = count($fields);
  for ($i=0; $i<$ns ; $i++) {
    $s->$fields[$i] = $stock[$i];
  }
  $s->updated = REQUEST_TIME;

  return $s;
}

/**
 * Given the current site-wide quotetype, generate the actual database fields
 * names currently active. These field names usually double as the keys for the
 * stock object.
 */
function _stockapi_get_fields($q = NULL) {
  if (!$q) $q = variable_get('stockapi_quotetype', 'basic');
  preg_match_all('/[a-z][0-9]?/', stockapi_get_quotetype($q), $yfields);
  $yfields = $yfields[0];

  $schema = drupal_get_schema('stockapi');
  $map = _stockapi_yahoo_map();

  $ns = count($yfields);
  $s = array();
  for ($i=0; $i<$ns ; $i++) {
    $dbname = $map[$yfields[$i]];
    $s[] = $dbname;
  }
  $s[] = 'updated';

  return $s;
}

/**
 * Return an array of Yahoo! fields keys mapped to the database field names.
 *
 * Note: Check out stockapi.install. We added a special key to the database
 * install schema to map the Yahoo! field name to our database field names. The
 * new key is named 'yahoo'.
 */
function _stockapi_yahoo_map() {
  static $map = NULL;

  if (!isset($map)) {
    $schema = drupal_get_schema('stockapi');
    foreach ($schema['fields'] as $name => $field) {
      if (isset($field['yahoo'])) {
        $y = $field['yahoo'];
        $map[$y] = $name;
      }
    }
  }
  return $map;
}

/**
 * Helper function to convert a feed from Yahoo! to an array.
 */
function _stockapi_parse_csv($data) {
  $stocks = array();
  $lines = explode("\r\n", trim($data));
  // Clean up the data.
  foreach ($lines as $key => $symbol) {
    $stock = explode(',', $symbol);
    foreach ($stock as $key => $value) {
      // Clean up and normalize the data
      $value = trim($value, '"');
      if ($value == '0.00') $value = 0;
      elseif ($value == '0.00%') $value = 0;
      $stock[$key] = $value;
    }
    $stocks[] = $stock;
  }
  return $stocks;
}

/**
 * Internal helper function to deal cleanly with various HTTP response codes.
 */
function _stockapi_request_failure($results) {
  switch ($results->code) {
    case '200': // Success!
    case '304': // Not modified, nothing to do.
      return FALSE;
    default:
      watchdog('stockapi', 'Failed to retrieve stock quotes with error: %error', array('%error' => $results->error));
      return TRUE;
  }
  return FALSE;
}

/**
 * Internal helper function to change text values returned for float
 * fields to '0'.
 */
function _stockapi_fix_floats($stock){
  $stock = (array)$stock;
  $schema = drupal_get_schema('stockapi');

  foreach ($schema['fields'] as $name => $field) {
    if ($field['type'] == 'float' && !is_numeric($stock[$name])) {
      $stock[$name] = 0;
    }
  }

  return (object)$stock;
}
